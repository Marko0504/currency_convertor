import { Component } from '@angular/core';
import { FormControl } from "@angular/forms";


@Component({
  selector: 'app-currency-convertor',
  templateUrl: './currency-convertor.component.html',
  styleUrls: ['./currency-convertor.component.css']
})
export class CurrencyConvertorComponent  {
  public vrednost1: any;
  public vrednost2: any;
  public currency_first: any;
  public currency_second: any;
  public heroForm:any;
  
  constructor() { 
    
  }

  currency1 = [
       {name: "EUR"},
       {name: "RSD"},  
    ];
    currency2 = [
      {name:"RSD"},
      {name:"EUR"},  
    ]
  getCurrencyFirst(currency: string){
      this.currency_first = currency;
      console.log(this.currency_first);
      if (currency == 'EUR'){
        this.currency2 =[
           {name: "RSD"},
           {name: "EUR"},
          ];
      }
      else if(currency == 'RSD'){
        this.currency2 =[
           {name: "EUR"},
           {name: "RSD"},
          ];
      }
      else{
        alert("GRESKA");
      }
  }
  getCurrencySecond(currency_second:string){
    this.currency_second = currency_second;
    console.log(this.currency_second);
  }

  change(vrednost1: any){
    var re = /^[a-zA-Z\s]+$/;  

    var OK = re.exec(vrednost1);  
        if (OK){
          window.alert('Please enter a number');
        }    
     else if(this.currency_first == 'EUR' && this.currency_second == 'RSD'){
          vrednost1 = vrednost1;
          this.vrednost2 = vrednost1 * 120.24;
          console.log(this.vrednost2);
      }
      else if(this.currency_first == 'RSD' && this.currency_second == 'EUR'){
        vrednost1 = vrednost1;
        this.vrednost2 = vrednost1/120.24;
        this.currency2 =[
           {name: "EUR"},
           {name: "RSD"},
          ];
      }
      else if(this.currency_first == 'RSD' && this.currency_second == 'RSD'){
        alert("Vec ste izabrali RSD valutu. Ne mozete da konvertujete u istu valutu");
     }
     else if(this.currency_first == 'EUR' && this.currency_second == 'EUR'){
        alert("Vec ste izabrali EUR valutu. Ne mozete da konvertujete u istu valutu");
     }
    else{
      alert("Izaberite valute");
    }
  }
  reset(){
    return this.vrednost1 = 0, this.vrednost2 = 0;
  }
}
